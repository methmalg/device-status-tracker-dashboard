import React, {Component} from 'react';
import './App.css';
import './css/MessageStyle.css';
import {BrowserRouter as Router, Switch} from 'react-router-dom';
import {EmptyLayout, LayoutRoute, MainLayout} from './components/Layout';
import * as Page from './pages';
import LeftNavigation from "./components/Layout/LeftNavigation";
import DashboardPage from "./pages/DashboardPage";

class App extends Component {
    messageCount = 0;

    constructor(props) {
        super(props);
        this.state = {
            message: []
        }
    }

    // render() {
    //     return (
    //         <Router>
    //             <Switch>
    //                 <MainLayout>
    //                     <LayoutRoute exact path="/dashboard" layout={EmptyLayout} component={Page.DashboardPage}/>)}/>
    //                     <LayoutRoute exact path="/history-data" layout={EmptyLayout} component={Page.HistoryDataPage}/>)}/>
    //                     <LayoutRoute exact path="/alerts" layout={EmptyLayout} component={Page.AlertsPage}/>)}/>
    //                     <LayoutRoute exact path="/reports" layout={EmptyLayout} component={Page.ReportsPage}/>)}/>
    //                 </MainLayout>
    //             </Switch>
    //         </Router>)
    // }

    render() {
        return (
            <div>
                <LeftNavigation/>
                <div>
                    <DashboardPage/>
                </div>
            </div>
        )
    }
}

export default App;
