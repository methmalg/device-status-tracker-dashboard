import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import PowerRoundedIcon from "@material-ui/icons/PowerRounded";
import PowerOffRoundedIcon from "@material-ui/icons/PowerOffRounded";
import DesktopWindowsRoundedIcon from "@material-ui/icons/DesktopWindowsRounded";
import DesktopAccessDisabledRoundedIcon from "@material-ui/icons/DesktopAccessDisabledRounded";
import React from "react";

class DataTable extends React.Component {

    displayPowerOn() {
        return <div>
            <p>(ON)</p>
            <DesktopWindowsRoundedIcon color={"action"}/>
        </div>
    }

    displayPowerOff() {
        return <div>
            <p>(OFF)</p>
            <DesktopAccessDisabledRoundedIcon color={"secondary"}/>
        </div>
    }

    devicePowerOn() {
        return <div>
            <p>(ON)</p>
            <PowerRoundedIcon color={"action"}/>
        </div>
    }

    devicePowerOff() {
        return <div>
            <p>(OFF)</p>
            <PowerOffRoundedIcon color={"secondary"}/>
        </div>
    }

    createRow() {
        this.getDataSet();
        return <TableRow>
            <TableCell align={"center"}>{this.props.message.lastUpdated}</TableCell>
            <TableCell align={"center"}>{this.props.message.deviceNumber}</TableCell>
            <TableCell align={"center"}>{(this.props.message.powerStatus) ? this.devicePowerOn() : this.devicePowerOff()}</TableCell>
            <TableCell align={"center"}>{(this.props.message.displayStatus) ? this.displayPowerOn() : this.displayPowerOff()}</TableCell>
            <TableCell align={"center"}>{this.props.message.batteryStatus}</TableCell>
            <TableCell align={"center"}>{this.props.message.tempStatus}</TableCell>
        </TableRow>
    }

    render() {
        return <div>
            <TableContainer component={Paper}>
                <Table aria-label="Status Data Table">
                    <TableHead>
                        <TableRow>
                            <TableCell align={"center"}>Last Updated At</TableCell>
                            <TableCell align={"center"}>Device Id</TableCell>
                            <TableCell align={"center"}>Device Power</TableCell>
                            <TableCell align={"center"}>Display</TableCell>
                            <TableCell align={"center"}>Battery Level</TableCell>
                            <TableCell align={"center"}>Temperature</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.getDataSet()}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    }

}

export default DataTable;
