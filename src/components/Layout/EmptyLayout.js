import Content from './Content';
import React from 'react';

const EmptyLayout = ({ children, ...restProps }) => (
  <main className="" {...restProps}>
    <Content fluid>{children}</Content>
  </main>
);

export default EmptyLayout;