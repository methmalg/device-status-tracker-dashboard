import {Content} from '../Layout/index';
import React from 'react';
import LeftNavigation from "./LeftNavigation";


class MainLayout extends React.Component {
  

    handleContentClick = event => {

    };

    render() {
        const { children } = this.props;

        return (
            <main className="">
                <LeftNavigation />
                <Content fluid onClick={this.handleContentClick}>
                </Content>
            </main>
        );
    }
}

export default MainLayout;
