import React from 'react';
import Grid from './Grid';
import WebSocket from "../../WebSocket";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import PowerRoundedIcon from "@material-ui/icons/PowerRounded";
import PowerOffRoundedIcon from "@material-ui/icons/PowerOffRounded";
import DesktopWindowsRoundedIcon from "@material-ui/icons/DesktopWindowsRounded";
import DesktopAccessDisabledRoundedIcon from "@material-ui/icons/DesktopAccessDisabledRounded";

class DeviceStatusGrid extends React.Component {

    rowData;

    constructor(props) {
        super(props);
        this.state = {
            columns: [],
            rows: [],
            message: [],
        }
    }
    componentDidMount() {
        this.loadGrid();
    }

    displayPowerOn() {
        return <>
            <p>(ON)</p>
            <DesktopWindowsRoundedIcon color={"action"}/>
        </>
    }

    displayPowerOff() {
        return <>
            <p>(OFF)</p>
            <DesktopAccessDisabledRoundedIcon color={"secondary"}/>
        </>
    }

    devicePowerOn() {
        return <>
            <p>(ON)</p>
            <PowerRoundedIcon color={"action"}/>
        </>
    }

    devicePowerOff() {
        return <>
            <p>(OFF)</p>
            <PowerOffRoundedIcon color={"secondary"}/>
        </>
    }

    checkDevicePowerStatus(power){
        if(power===true){
            this.devicePowerOn()
        }
        else {
            this.devicePowerOff();
        }
    }

    setMessage = (message) => {
        console.log(message);
        this.setState({message});
        this.addToLocalStorage();
        this.loadGrid();
    }

    addToLocalStorage() {
        const statusDataSet = {
            device: this.state.message.deviceNumber,
            timestamp: this.state.message.lastUpdated,
            power: this.state.message.powerStatus,
            display: this.state.message.displayStatus,
            battery: this.state.message.batteryStatus,
            temp: this.state.message.tempStatus,
        }
        localStorage.setItem(this.state.message.deviceNumber, JSON.stringify(statusDataSet))
    }

    getAllKeys() {
        return Object.keys(localStorage);
    }

    getDataSet() {
        const keyList = this.getAllKeys();
        const dataSet = [];
        for (let i = 0; i < keyList.length; i++) {
            if (keyList[i] === "undefined") {
                continue;
            }
            dataSet.push(JSON.parse(localStorage.getItem(keyList[i])));
        }
        return dataSet;
    }

    renderPowerStatus = (params) => {
            if(params.data.powerStatus ===true){
                return this.devicePowerOn();
            }
            else{
                return this.devicePowerOff()
            }
    }

    renderDisplayStatus = (params) => {
            if (params.data.displayStatus === true) {
                return this.displayPowerOn();
            } else {
                return this.displayPowerOff()
            }
    }

    displayPowerOn() {
        return <div>
            <DesktopWindowsRoundedIcon color={"action"}/>
        </div>
    }

    displayPowerOff() {
        return <div>
            <DesktopAccessDisabledRoundedIcon color={"secondary"}/>
        </div>
    }

    devicePowerOn() {
        return <div>
            <PowerRoundedIcon color={"action"}/>
        </div>
    }

    devicePowerOff() {
        return <div>
            <PowerOffRoundedIcon color={"secondary"}/>
        </div>
    }

    loadGrid = () => {

        this.rowData = this.getDataSet();

        const columns = [
            {headerName: 'Device Id', field: 'deviceId', sortable: true, filter: true, width: 200, align:'center'},
            {headerName: 'Power', field: 'powerStatus', cellRendererFramework:this.renderPowerStatus, sortable: true, filter: true, width: 200, align:'center'},
            {headerName: 'Display', field: 'displayStatus', cellRendererFramework:this.renderDisplayStatus, sortable: true, filter: true, width: 200, align:'center'},
            {headerName: 'Battery Level (mAh)', field: 'battery', sortable: true, filter: true, width: 200, align:'center'},
            {headerName: 'Temperature (°C)', field: 'temperature', sortable: true, filter: true, width: 200, align:'center'},
            {headerName: 'Last Updated At', field: 'timestamp', sortable: true, filter: true, width: 300, type:'dateTime', align:'center'}
        ]
        const rows = [];

        this.rowData.map(deviceDataSet => {

            rows.push({
                timestamp: deviceDataSet.timestamp,
                deviceId: deviceDataSet.device,
                powerStatus: deviceDataSet.power,
                displayStatus: deviceDataSet.display,
                battery: deviceDataSet.battery,
                temperature: deviceDataSet.temp,
            })
        })
        this.setState({columns, rows});
    }

    render() {
        return (
            <>
                <WebSocket onReceiveMessage={this.setMessage}/>
                <div
                    className="ag-theme-alpine"
                    style={{
                        height: '200px',
                        width: 'auto'
                    }}
                >
                    <Card>
                        <CardContent>
                            <Grid
                                columnDefs={this.state.columns}
                                rowData={this.state.rows}
                                pagination
                                pageSize={10}
                                rowsPerPageOptions={[10, 25, 50]}
                            />
                        </CardContent>
                    </Card>
                </div>
            </>
        );
    }
}

export default DeviceStatusGrid;
