import React from "react";
import DeviceStatusGrid from "../components/Grid/DeviceStatusGrid";

class DashboardPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <div>
                <div>
                    <DeviceStatusGrid/>
                </div>
            </div>
        )
    }
}

export default DashboardPage;
