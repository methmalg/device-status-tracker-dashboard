export { default as DashboardPage } from './DashboardPage';
export { default as HistoryDataPage } from './HistoryDataPage';
export { default as AlertsPage } from './AlertsPage';
export { default as ReportsPage } from './ReportsPage';
