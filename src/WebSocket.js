import React from 'react';
import SockJsClient from 'react-stomp';

const topic = '/topic/status';
const WEBSOCKET_URI = 'http://localhost:8080/websocket-status/';

class Websocket extends React.Component {

    receiveMessage = (msg) => {
        this.props.onReceiveMessage(msg);
    }

    render() {
        return (
            <div>
                <SockJsClient
                    url={WEBSOCKET_URI}
                    topics={[topic]}
                    onConnect={() => {
                        console.log("Connected");
                    }}
                    onDisconnect={() => {
                        console.log("Disconnected");
                    }}
                    onMessage={(msg) => this.receiveMessage(msg)}
                    ref={() => {
                    }}
                />
            </div>
        );
    }
}

export default Websocket;
